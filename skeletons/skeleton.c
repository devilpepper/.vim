/* Notes: */
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Do interesting stuff.\n\n"
"   --in     FILE   use FILE for input.\n"
"   --out    FILE   use FILE for output.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[]) {
	/* define long options */
	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	/* process options: */
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hi:o:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 'i':
				break;
			case 'o':
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	return 0;
}
