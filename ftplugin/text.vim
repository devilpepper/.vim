" Default settings for text files
setlocal textwidth=78

" Borrow some stuff from the markdown file
setlocal comments=fb:*,fb:-,fb:+,n:> commentstring=
" TODO: is there any meaningful setting of commentstring for a text file?
" I cannot think of one, so I'll leave it blank.
setlocal formatoptions+=tcqn
setlocal formatlistpat=^\\s*\\d\\+\\.\\s\\+\\\|^[-*+]\\s\\+

setlocal spell spelllang=en_us
"setlocal complete+=kspell
" Usually there are too many spelling options to be useful.

setlocal foldmethod=marker

nnoremap yq yypVr-
nnoremap yQ yypVr=

" some surround mappings from markdown:"{{{
" All mappings defined here are buffer-local, so the following
" statements need to be executed every time:
let b:surround_66 = "**\r**"
let b:surround_69 = "*\r*"
let b:surround_67 = "`\r`"
let b:surround_36 = "$\r$"

" visual mode:
xmap <buffer> <M-b> <Plug>VSurroundB
xmap <buffer> <M-c> <Plug>VSurroundC
xmap <buffer> <M-e> <Plug>VSurroundE
xmap <buffer> <M-m> <Plug>VSurround$

" normal mode:
nmap <buffer> <M-b> <Plug>YsurroundewB
nmap <buffer> <M-c> <Plug>YsurroundewC
nmap <buffer> <M-e> <Plug>YsurroundewE
nmap <buffer> <M-m> <Plug>Ysurroundew$

" insert mode:
imap <buffer> <M-b> <Plug>IsurroundB
imap <buffer> <M-c> <Plug>IsurroundC
imap <buffer> <M-e> <Plug>IsurroundE
imap <buffer> <M-m> <Plug>Isurround$
"}}}
"{{{ use pandoc to create pdf

nnoremap <buffer> <localleader>c :call <SID>runPandoc(0)<CR>
nnoremap <buffer> <localleader>C :call <SID>runPandoc(1)<CR><C-L>

"}}}

if exists("g:loaded_text")
	finish
endif
let g:loaded_text = 1

function s:runPandoc(startpdf)
	silent update
	let outfile = "/tmp/" . expand("%:t:r") . ".pdf"
	echon "Running pandoc..."
	exe "!pandoc " . expand("%:p") . " -o " . outfile
	if v:shell_error
		echohl WarningMsg
		echomsg "pandoc failed."
		echohl None
	else
		redraw
		echon "pandoc successful."
		if a:startpdf == 1
			if filereadable('/usr/bin/mimeo')
				silent execute "!mimeo " . outfile . " &> /dev/null"
			else
				silent execute "!xdg-open " . outfile
			endif
		endif
	endif
endfunction
