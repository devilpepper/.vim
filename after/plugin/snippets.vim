" custom configuration for snippets.

" load markdown stuff for text and mail:
call ExtractSnipsFile(g:snippets_dir . "markdown.snippets", "text")
call ExtractSnipsFile(g:snippets_dir . "markdown.snippets", "mail")


" vim:sw=4:ff=unix:ft=vim
