" Set the usual control statements (if, while, let, etc.) to be
" highlighted as statements in vim files.
syn keyword vimStatement if el[se] elsei[f] en[dif] let unl[et] function endf[unction] wh[ile] endw[hile] for in endfo[r] con[tinue] brea[k] try endt[ry] cat[ch] fina[lly] th[row]

" Make sure you get the abbrev versions of the commands right (with the []
" notation).

syn keyword vimNote containedin=vimLineComment   NOTE IDEA  contained
hi def link vimNote Underlined
hi Underlined	gui=bold cterm=bold
