syn keyword pythonNote containedin=pythonComment  NOTE IDEA contained

hi def link pythonNote Underlined

hi Statement	gui=bold cterm=bold
hi PreProc		gui=bold cterm=bold
hi Underlined	gui=bold cterm=bold
