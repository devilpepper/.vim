" for notes:
syn keyword shNote contained NOTE IDEA
syn cluster shCommentGroup add=shNote
hi def link shNote Underlined
hi Underlined	gui=bold cterm=bold


" vim:sw=4:ff=unix:ft=vim

