" Highlight Class and Function names

"if !exists("g:loaded_aftersync")
	syn match    cCustomParen    "(" contains=cParen contains=cCppParen
	syn match    cCustomFunc     "\w\+\s*(" contains=cCustomParen
	syn match    cCustomScope    "::"
	syn match    cCustomClass    "\w\+\s*::" contains=cCustomScope
	let g:loaded_aftersync = 1
"endif

" for notes:
syn keyword cNote contained NOTE IDEA
syn cluster cCommentGroup add=cNote
hi def link cNote Underlined

" always put the highlighting links back.
hi def link cCustomFunc  Function
hi def link cCustomClass Function

" modifications to some of the standard items:
hi Statement	gui=bold cterm=bold
hi PreProc		gui=bold cterm=bold
hi Underlined	gui=bold cterm=bold
