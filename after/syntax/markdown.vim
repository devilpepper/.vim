" Use the better pandoc markdown syntax file.
" let g:pandoc_no_folding = 1
" let g:pandoc_use_hard_wraps = 1
" let g:pandoc_auto_format = 0
" let g:pandoc_no_empty_implicits = 1
" let g:pandoc_no_spans = 1
" set syntax=pdc
" syntax is always local, so no need to specify.
set syntax=pandoc
