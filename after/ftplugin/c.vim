setlocal shiftwidth=4
setlocal foldmethod=syntax
setlocal formatoptions-=o
" to exclude documentation directories:
setlocal grepprg=ack\ --ignore-dir=html

nnoremap <buffer> <localleader>c :update<CR>:make<CR>
nnoremap <buffer> <localleader>k :make clean<CR>
nnoremap <buffer> ,q =ap
nnoremap <buffer> ,d :call <SID>gotoFuncDef()<CR>

" ctags configuration {{{

setlocal tags+=./.tags
setlocal completeopt-=preview

" Create a tags file for your c/cpp code:
nnoremap <buffer> <S-C-F12> :!ctags -R --c++-kinds=+pl --fields=+iaS --extra=+q -f .tags .<CR>
" A couple of interesting things to do with it:
" C-] - go to definition
" C-T - Jump back from the definition.
" C-W C-] - Open the definition in a horizontal split
command! -buffer -nargs=0 MT !ctags -R --c++-kinds=+pl --fields=+iaS --extra=+q -f .tags .
command! -buffer -nargs=? -complete=file TagConcat call s:appendTags(<f-args>)

" TODO: if you have some tags you've concatenated (which may have been some
" non-trivial work) and then you want to refresh the tags from your local
" codebase, it will end up trashing the external tags you brought in via
" TagConcat.  Not sure what the fix is, but something needs to be done.

"}}}

" surroundings {{{
let b:surround_67 = "/* \r */"

" visual mode:
xmap <buffer> <M-c> <Plug>VSurroundC

" normal mode:
nmap <buffer> <M-c> <Plug>YsurroundewC

" insert mode:
imap <buffer> <M-c> <Plug>IsurroundC
"}}}

" prevent duplicate function definitions:
if exists("g:loaded_after_c")
	finish
endif
let g:loaded_after_c = 1

" function to generate tags for current file or directory, and
" concatenate them to the .tags file of the working directory.
function s:appendTags(...)
	let targetFile = expand("%:p")
	let cmdbase = "!ctags --append --c++-kinds=+pl --fields=+iaS --extra=+q"
				\ . " -f .tags"
	for i in range(a:0)
		let cmdbase = cmdbase . " " . a:000[i]
	endfor
	exe cmdbase . " " . targetFile
endfunction

" Rationale: the pattern matching for &tags is strange.  You cannot pattern
" match the filename!  Only directories.  So, you have to do something like:
" tags+=./.tags/**/tags  which is looking like a pain.  Hence we instead have
" the above function which will concatenate tags from the current file to the
" local .tags file (in your working directory).

function s:gotoFuncDef()
	silent! exe ':vimgrep /\<' . expand('<cword>') . '\_s*(\_[^)]*)\_s*{/ *.c *.h *.cpp'
	if errmsg =~ "^E480" " no match; try preprocessor macro
		silent! exe ':vimgrep /^\s*#define\s\+' . expand('<cword>') . '\>/ *.c *.h *.cpp'
	endif
endfunction

" get doxygen docs for loaded buffers
function s:documentBuffers()
	let filelist = ""
	let bufnums = range(0, bufnr('$'))
	for k in bufnums
		if bufloaded(k)
			let filelist = filelist . " " . shellescape(bufname(k))
		endif
	endfor
	exe "!/home/wes/repos/misc/makefiles/quickdocs.sh" . filelist
endfunction
command DoxygenBufs call s:documentBuffers()
