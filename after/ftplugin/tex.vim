" local settings"{{{
setlocal shiftwidth=2
setlocal foldmethod=marker
setlocal foldmarker={{{[[[,]]]}}}
setlocal spell spelllang=en_us
setlocal textwidth=78
setlocal completefunc=CompleteLaTeXMacros
setlocal formatoptions+=tcqr
" this should stop cluttering the screen with massive
" latex compiler output.  I don't think it will have any
" adverse effects.  We'll see.
setlocal shellpipe=>
" lcd into directory of latex file?  Rationale: I think this will
" solve some annoying problems with one line of code, and will rarely
" cause any inconvenience.  Should be a win.
lcd %:p:h

" I think this should actually be an omni function.
" NOTE: can't set iskeyword here because it will be overwritten by that
" setting from the syntax file (sourced after this, apparently). Setting it in
" after/syntax works fine though.
"}}}

" LaTeX-specific mappings:"{{{
" All mappings defined here are buffer-local, so the following
" statements need to be executed every time:
" surroundings"{{{
let b:surround_36 = "\\(\r\\)"
let b:surround_35 = "\\[\r\\]"
let b:surround_66 = "\\textbf{\r}"
let b:surround_69 = "\\emph{\r}"
let b:surround_85 = "^{\r}"
let b:surround_68 = "_{\r}"
let b:surround_73 = "\\ignore{%{{{[[[\n\r\n}%]]]}}}"

" Note: stuff like the ignore mapping that you don't use all that often
" is probably not necessary.  You can use the default mappings from
" surround.vim to get the job done if you can just remember the letter
" that you assigned to it.

" visual mode:
xmap <buffer> <M-m> <Plug>VSurround$
xmap <buffer> <S-M-m> <Plug>VSurround#
xmap <buffer> <M-b> <Plug>VSurroundB
xmap <buffer> <M-e> <Plug>VSurroundE
xmap <buffer> <M-u> <Plug>VSurroundU
xmap <buffer> <M-d> <Plug>VSurroundD
xmap <buffer> ,C <Plug>VSurroundI

" normal mode:
nmap <buffer> <M-m>   <Plug>Ysurroundew$
nmap <buffer> <S-M-m> <Plug>Ysurroundew#
nmap <buffer> <M-b> <Plug>YsurroundewB
nmap <buffer> <M-e> <Plug>YsurroundewE
nmap <buffer> <M-u> <Plug>YsurroundewU
nmap <buffer> <M-d> <Plug>YsurroundewD

" insert mode:
imap <buffer> <M-m> <Plug>Isurround$
imap <buffer> <S-M-m> <Plug>Isurround#
imap <buffer> <M-b> <Plug>IsurroundB
imap <buffer> <M-e> <Plug>IsurroundE
imap <buffer> <M-u> <Plug>IsurroundU
imap <buffer> <M-d> <Plug>IsurroundD
"}}}
" searches"{{{
" I think these are useful enough to warrant mappings involving
" the home row:
nnoremap <buffer> <silent> <M-l> :call search('\\(\_.\{-}\\)\<BAR>\\\[\_.\{-}\\]\<BAR>\$\_.\{-}\$','sW')<CR>
nnoremap <buffer> <silent> <M-j> :call search('\\(\_.\{-}\\)\<BAR>\\\[\_.\{-}\\]\<BAR>\$\_.\{-}\$','sbW')<CR>
"inoremap <buffer> <silent> <C-n> <C-O>:call search('{\\|(\\|[',"bW")<CR>
" Note: supertab failed to use *nore*map.  unreal.
" These mappings are like gD for identifiers, but works for \labels.
" The first will not jump, but open the quickfix list; the second
" does the opposite, and only searches for the label definition.
nnoremap <buffer> <silent> gl yiB:vimgrep /<C-R>"/j *.tex *.ltx<CR>:copen<CR>
nnoremap <buffer> <silent> gL yiB:vimgrep /label{<C-R>"/ *.tex<CR>
" NOTE if you include the *.ltx extension, and there are no .ltx files in the
" directory, then it raises an error x_x
" Note: you can make the above recursive by using **/*.tex as the pattern.
"}}}
" misc"{{{
inoremap <buffer> <M-y> <Space>+<Space>
inoremap <buffer> <S-M-n> <C-G>u<CR>\item 
nnoremap <buffer> <silent> ,q :call <SID>smallLatexFormat("gq")<CR>
nnoremap <buffer> <silent> ,Q =ap
nnoremap <buffer> <silent> ,W gwap
" TODO: add an insert mode formatting?
" inoremap <buffer> <silent> <S-M-q> <C-O>:call <SID>smallLatexFormat("gw")<CR>
nnoremap <buffer> <silent> <M-t>u :call <SID>RemoveLaTeXTag()<CR>
"}}}
" compilation / makefiles"{{{
"nnoremap <buffer> <localleader>p :w<CR>:!make<CR>
nnoremap <buffer> <localleader>c :call <SID>runlatexMake(0,0)<CR>
nnoremap <buffer> <localleader>C :call <SID>runlatexMake(1,0)<CR><C-L>
nnoremap <buffer> <localleader><localleader>c :call <SID>runlatexMake(0,0,1)<CR>
nnoremap <buffer> <localleader>p :call <SID>runlatexMake(0,1)<CR>
nnoremap <buffer> <localleader>P :call <SID>runlatexMake(1,1)<CR><C-L>
nnoremap <buffer> <localleader><localleader>p :call <SID>runlatexMake(0,1,1)<CR>
nnoremap <buffer> <localleader>k :call <SID>latexClean()<CR>
nnoremap <buffer> <localleader>K :call <SID>latexSuperClean()<CR>
nnoremap <buffer> <localleader>v :call <SID>runlatexMakeVerbose()<CR>
command! -nargs=? -complete=file Setmain call s:setLatexMainFile(<f-args>)
" convenient synonym- mnemonic: 'M_ake this M_ain'
command! -nargs=? -complete=file MM call s:setLatexMainFile(<f-args>)
command! -nargs=0 Unsetmain let g:latexMainFileName = ""
"}}}
" Plug-in mappings"{{{
" LaTeXBox"{{{
" Begin/end pairs: movement / selection"{{{
nmap <buffer> % <Plug>LatexBox_JumpToMatch
xmap <buffer> % <Plug>LatexBox_JumpToMatch
vmap <buffer> ee <Plug>LatexBox_SelectCurrentEnvInner
vmap <buffer> ae <Plug>LatexBox_SelectCurrentEnvOuter
omap <silent> <buffer> ee :normal vee<CR>
omap <silent> <buffer> ae :normal vae<CR>
vmap <buffer> em <Plug>LatexBox_SelectInlineMathInner
vmap <buffer> am <Plug>LatexBox_SelectInlineMathOuter
omap <silent> <buffer> em :normal vem<CR>
omap <silent> <buffer> am :normal vam<CR>
"}}}
" Environment insertion / wrapping / changing"{{{
nmap <buffer> <F6>		<Plug>LatexChangeEnv
vmap <buffer> <M-v>		<Plug>LatexEnvWrapSelection
vmap <buffer> <S-M-v>	<Plug>LatexEnvWrapFmtSelection
imap <buffer> <M-v>		<Space><BS><Esc>V<Plug>LatexEnvWrapSelection
nmap <buffer> <M-v>		V<Plug>LatexEnvWrapSelection
" inoremap <buffer> (( 		\eqref{
"}}}
"}}}
"}}}
" Auto-completion mappings"{{{
" Hack-tastic, but it will do for now.
inoremap <buffer> <S-M-k> <C-o><Esc><C-n>
" Autolist bibliography entries
inoremap <buffer> <expr> <M-c> ShowBibList(0,1)
inoremap <buffer> <expr> <S-M-c> ShowBibList(1,1)
nnoremap <buffer> <expr> <M-c> ShowBibList(1,0)
inoremap <buffer> <expr> <M-r> ShowRefList(0)
inoremap <buffer> <expr> <S-M-r> ShowRefList(1)
"}}}
"}}}

" Temporary test mappings:"{{{
" nnoremap <S-M-R> :call <SID>loadLatexBibList()<CR>
" nnoremap <S-M-R> :call <SID>findBibliographies()<CR>
" nnoremap <S-M-R> :call <SID>loadLatexLabelList()<CR>
"}}}
if exists("g:loaded_after_tex")
	finish
endif
let g:loaded_after_tex = 1

" the following variable will accumulate any errors encountered
" during the loading / parsing of a latex file.
let g:latex_plugin_errlog = ""

" configuration variables"{{{
" default makefile:
if has('win32') || has('win64')
	let s:defltxmakefile = expand('~/Documents/miscsvn/scripts/latex_makefile')
else
	let s:defltxmakefile = expand('~/repos/misc/makefiles/latex_makefile')
endif
" LaTeX macro list: set this variable to a file of macros
" (newline delimited, including the backslashes) that you'd like to complete:
" Different locations for linux vs. windows:
if filereadable(expand('~/vimfiles/resources/complete/ltxmacros.txt'))
	let s:LaTeXMacroFile = expand('~/vimfiles/resources/complete/ltxmacros.txt')
else
	let s:LaTeXMacroFile = expand('~/.vim/resources/complete/ltxmacros.txt')
endif

" make sure we only read the file once: (taken care of above, but this might
" move around...)
if !exists('g:latexMacroList')
	let g:latexMacroList = []
	for line in readfile(s:LaTeXMacroFile)
		call add(g:latexMacroList,line)
	endfor
endif
" list of bibliography file locations.  These will be searched in the order
" that they are provided until a match is found. path expansion is done later.
" TODO: can you make some system calls to texlive to figure
" this stuff out instead?  Probably too much work...
if has('win32') || has('win64')
	let g:latexBibDirectories = ['./','~/documents/work/texmf/bibtex/bib/']
	call add(g:latexBibDirectories,'~/documents/ccny/texmf/bibtex/bib/')
else
	let g:latexBibDirectories = ['./','~/repos/texmf/bibtex/bib/']
	call add(g:latexBibDirectories,'~/repos/ccny/research/texmf/bibtex/bib/')
endif
let g:latexBibList = []
let g:latexRefList = []
"}}}
" function definitions"{{{
" primitive latex 'formatting' "{{{
function s:smallLatexFormat(formatop)
	normal! mz
	let fmtend = search('^\s*\\\|^$\|^\s*%',"nW")
	if fmtend == 0 " search hit end of file
		let fmtend = line("$")
	else
		let fmtend = fmtend - 1
	endif
	let fmtbegin = search('^\s*\\\|^$\|^\s*%',"bcW")
	if fmtbegin == 0 " search hit top
		let fmtbegin = 1
	endif
	" TODO: the above searches should really look for specific macros, like
	" \begin, \end, \section, \subsection, etc. If you have say, a math macro
	" that winds up on the first column, this will stop the formatting.
	" format \item lines, but not \otherstuff.
	if getline(".") !~ '^\s*\\item'
		let fmtbegin = fmtbegin + 1
	endif
	" TODO: this is a temporary patch; fix the above issue and take this out:
	if fmtbegin > fmtend
		return
	endif
	exe "normal! " . fmtbegin . "gg"
	normal! V
	exe "normal! " . fmtend . "gg"
	exe "normal! " . a:formatop
	" try to get back to approximately the same spot:
	normal! `z
endfunction
"}}}
" An attempt at completion of macros/bib/labels "{{{
" Basic completion function"{{{
" Documentation: c.f. *E839*
" Note: this is kind of hacky, but I don't see a great way around it:
let s:shouldCompleteBib = 0
let s:shouldCompleteRefs = 0
function! CompleteLaTeXMacros(findstart, base)
	if a:findstart
		" Find the start of the word.  For us, this is defined by:
		" 1. whitespace, or something non-keyword like
		" 2. running into a backslash, denoting the start of a macro.
		" NOTE: line will be 0-based, while the columns of text are 1-based.
		" Hence, we have to do a little translation. Also, when you are in
		" insert mode, col('.') will give the column you are about to edit;
		" so the beginning of what you want to check is an index before that.
		let line = getline('.')
		let start = col('.') - 1
		while start > 0 && line[start - 1] =~ '\a'
			let start -= 1
		endwhile
		if start && line[start - 1] =~ '\\'
			return start - 1
		endif
		return start
	endif
	" find keywords matching with "a:base"
	" call confirm("The base was " . a:base)
	let s:res = []
	let s:count = 0
	" Are we completing a macro, or a bibliograpy item?
	" Notes"{{{
	" NOTE: for some reason, when user completion is invoked via a macro, say
	" <M-c> in this case, then even if text has been added via the macro, it
	" will not show up the variable a:base. I think this has something to do
	" with the <expr> mapping disabling any changing of the buffer text.  We
	" resort to a hack which sets a flag that's checked / rest here.  Not so
	" bad though...
	"if a:base =~ escape('\cite{','\')
	""}}}
	if s:shouldCompleteBib
		let complList = g:latexBibList
		let s:shouldCompleteBib = 0
	elseif s:shouldCompleteRefs
		let complList = g:latexRefList
		let s:shouldCompleteRefs = 0
	else
		let complList = g:latexMacroList
	endif
	" TODO: make use of the preview window by adding the title
	" and year from the bibliography entries.
	" To simlify the code here, you will probably want to just
	" construct the dictionaries when you are extracting everything
	" from the bib files.  Note: this will require more work than it
	" is probably worth.  You can't use vimgrepadd anymore- you'd have
	" to actually parse the bibliography file yourself to extract the
	" titles and such.
	for m in complList
		if m =~? '^' . escape(a:base,'\')
			call add(s:res, {"word": m, "icase": 1})
			let s:count = s:count+1
		endif
	endfor
	return s:res
endfunction
"}}}
" Build list for label completion"{{{
" Idea: run through each of the files in findTexDependencies() and vimgrepadd the
" results of /\\label{\(\i\|[-+:]\)*}.  Then go through as usual and strip out
" what you need into a list.  It feels like this could be abstracted...
function s:loadLatexLabelList()
	" This will re-compute the list of labels every time it is called,
	" so only call it when you think you need to.
	let inputlist = s:findTexDependencies(g:latexMainFileName)
	call add(inputlist,g:latexMainFileName)
	let g:INPL = inputlist
	call setqflist([])
	for fname in inputlist
		try
			exe 'vimgrepadd /\\label{\(\i\|[-+:]\)*}/gj ' . fnameescape(fname)
		catch
			continue
		endtry
	endfor
	let g:latexRefList = map(getqflist(),'get(v:val,"text")')
	let g:latexRefList = map(g:latexRefList,'substitute(v:val,''.\{-}\\label{\(\%(\i\|[-+:]\)*\)}.*'',''\1'','''')')
endfunction
"}}}
" Build list for bibliography completion"{{{
function s:loadLatexBibList()
	" this is set to rebuild the citation list every time,
	" so only call it when you think you need to.
	let shortbibnames = s:findBibliographies()
	call setqflist([])
	for bibname in shortbibnames
		let fullbibfile = ""
		for directory in g:latexBibDirectories
			if filereadable(expand(directory . bibname . ".bib"))
				let fullbibfile = expand(directory . bibname . ".bib")
				break
			endif
		endfor
		if fullbibfile == ""
			echohl WarningMsg
			echo "Bibliography file " . bibname . " not found"
			echohl None
			continue
		endif
		" now read the bibliography file
		try
			exe 'vimgrepadd /^@\a\+\s*{\s*\([-_+:a-z0-9A-Z]\+\)/j ' . fnameescape(fullbibfile)
			" TODO: I think you can clean up this expression using the \i
			" characters (c.f. what was done for labels).
		catch
			" TODO: would be strange to have an empty bib file...
			" send a message in this case?
			continue
		endtry
	endfor
	let g:latexBibList = map(getqflist(),'get(v:val,"text")')
	let g:latexBibList = map(g:latexBibList,'substitute(v:val,''^@\a\+\s*{\s*\([-_+:a-z0-9A-Z]\+\),.*'',''\1'','''')')
endfunction
"}}}
" Find bibliographies from main / current file "{{{
" @return: a list containing the bibliographies used by the main file,
" or the current file if no main is set.  Returns [] if none are found.
function s:findBibliographies()
	let mainfile = (exists('g:latexMainFileName') && g:latexMainFileName != "")?
				\ g:latexMainFileName : expand("%")
	try
		silent exe 'vimgrep /^\s*\\bibliography{\_.\{-}}/j ' . fnameescape(mainfile)
	catch
		return []
	endtry
	" NOTE: Even though the regular expression is correct, vimgrep will not
	" add multiple lines to the output, hence this may still fail when
	" bibliographies are spread across several lines.
	" The code below just uses one \bibliography line.
	let bibline = map(getqflist(),'get(v:val,"text")')[0]
	" trim the irrelevant stuff:
	let bibline = substitute(bibline,'\\bibliography\|{\|}\|\s*',"","g")
	return split(bibline,',')
endfunction
"}}}
" Helper function for completing bibliography items"{{{
function ShowBibList(includeTag,curMode)
	if g:latexBibList == []
		return "~\\cite{}\<Esc>i"
	endif
	let s:shouldCompleteBib = 1
	if a:includeTag
		if a:curMode == 0 " normal
			return "ea,\<C-X>\<C-U>\<C-P>"
		else
			return ",\<C-X>\<C-U>\<C-P>"
		endif
	endif
	return "~\\cite{}\<Esc>i\<C-X>\<C-U>\<C-P>"
endfunction
"}}}
" Helper function for completing label items"{{{
function ShowRefList(useEqRef)
	if g:latexRefList == []
		return "~\\ref{}\<Esc>i"
	endif
	let s:shouldCompleteRefs = 1
	if a:useEqRef
		return "~\\eqref{}\<Esc>i\<C-X>\<C-U>\<C-P>"
	endif
	return "~\\ref{}\<Esc>i\<C-X>\<C-U>\<C-P>"
endfunction
"}}}
" Supertab configuration"{{{
" Documentation: see
" *supertab-contextdiscover*
" *supertab-contextexample*
" *supertab-completioncontexts*
function MyLaTeXContext()
	let line = getline('.')
	let start = col('.') - 1
	while start > 0 && line[start - 1] =~ '\a\|\\'
		let start -= 1
	endwhile
	" check for file name completion first.
	if line[start-1] == '/'
		return
	endif
	if line[start] =~ '\\'
		if start == col('.') - 2
			return "\<c-x>\<c-u>\<c-p>"
		else
			return "\<c-x>\<c-u>"
		endif
	elseif line[start] =~ '\a'
		return "\<c-p>"
	endif
	" no return will result in the evaluation of the next
	" configured context
endfunction
let g:SuperTabCompletionContexts =
\ ['MyLaTeXContext', 's:ContextText', 's:ContextDiscover']
"}}}
"}}}
" Makefile stuff; setting the main file"{{{
" Sets the main file to the first parameter, or if not supplied, to the
" current buffer's file.
" TODO this does not work so well when the main file is not in the current
" working directory.  Try to fix by using absolute paths, or perhaps by 
" first performing a local change dir (:LCD) first.
function s:setLatexMainFile(...) "{{{
	let g:latex_plugin_errlog = ""
	redir =>> g:latex_plugin_errlog
	if a:0 && a:1 != ''
		let g:latexMainFileName = a:1
	else
		let g:latexMainFileName = expand("%:t")
	endif
	" re-compile the completion lists:
	call s:loadLatexBibList()
	call s:loadLatexLabelList()
	redir END
endfunction
"}}}
function s:getLatexMainFile() "{{{
	let localMainFileName = expand("%")
	if exists('g:latexMainFileName') && g:latexMainFileName != ""
		let localMainFileName = g:latexMainFileName
	endif
	return localMainFileName
endfunction
"}}}
" clean intermediate files {{{
function s:latexClean()
	exe "make -f " . s:defltxmakefile . " clean"
	redraw
	echon "Clean finished."
endfunction

function s:latexSuperClean()
	exe "make -f " . s:defltxmakefile . " superclean"
	redraw
	echon "SuperClean finished."
endfunction
"}}}
function s:findTexDependencies(mainfile) "{{{
	" Return the list of files referenced in \input
	" and \include statements in the main file
	try
		exe 'vimgrep /^\s*\\input\>\|^\s*\\include\>/j ' . fnameescape(a:mainfile)
	catch
		return []
	endtry
	let inputlist = map(getqflist(),'get(v:val,"text")')
	" strip comments from each item of the list:
	call map(inputlist,"substitute(v:val,'%.*','','g')")
	" strip the \input and \include tags:
	call map(inputlist,"substitute(v:val,'\\\\input\\s*{\\s*\\|\\\\include\\s*{\\s*\\|\\s*}','','g')")
	" strip whitespace from the ends; add .tex extensions:
	call map(inputlist,"substitute(v:val,'^\\s*','','g')")
	call map(inputlist,"substitute(v:val,'\\>\\s*\\_$','.tex','g')")
	return inputlist
endfunction
"}}}
" save yourself from being stupid: "{{{
function s:checkPossibleMainFile(mainfile)
	try
		exe 'vimgrep /^\s*\\begin{document}/j ' . fnameescape(a:mainfile)
	catch
		return 0
	endtry
	return 1
endfunction
"}}}
" run make externally to Vim: "{{{
function s:runlatexMakeVerbose()
	silent update
	let mainfile = s:getLatexMainFile()
	if !s:checkPossibleMainFile(mainfile)
		echon "Are you sure " . mainfile . " is the main file??"
		return
	endif
	let maindeps = join(s:findTexDependencies(mainfile))
	let makestring = "!make -f " . s:defltxmakefile . " main=" . fnamemodify(mainfile,":r")
	if maindeps != ""
		let makestring = makestring . " deps='" . maindeps . "'"
	endif
	exe makestring
endfunction
"}}}
" Main makefile execution / pdf display "{{{
function s:runlatexMake(startpdf,trylocal,...)
	" if startpdf is true, try to open the pdf after compiling.
	" if trylocal is true, attempt to use local makefile before the global
	" The third parameter, if present, asks to force compilation
	" by touching the main file before running make.
	silent update
	let mainfile = s:getLatexMainFile()
	if !s:checkPossibleMainFile(mainfile)
		echon "Are you sure " . mainfile . " is the main file??"
		return
	endif
	if a:0 && a:1
		call system('touch ' . mainfile)
	endif
	let maindeps = join(s:findTexDependencies(mainfile))
	let makestring = "make! -f " . s:defltxmakefile . " main=" . fnamemodify(mainfile,":r")
	if maindeps != ""
		let makestring = makestring . " deps='" . maindeps . "'"
	endif
	if a:trylocal == 1 && filereadable("makefile")
		let makestring = "make!"
		echon "Compiling with local makefile"
	else
		echon "Compiling with " . s:defltxmakefile
	endif
	call s:SetLatexEfm()
	exe makestring
	if v:shell_error
		if len(getqflist()) > 0
			copen
			echohl WarningMsg
			echomsg "Build failed."
			echohl None
		endif
	else
		cclose
		redraw "get rid of annoying message about hitting return...
		echon "Build succeeded."
		if a:startpdf == 1
			if has('win32') || has('win64')
				silent execute "! start " . fnamemodify(mainfile,":p:r") . ".pdf"
				" Note: for some reason, the space between the ! and the start
				" makes things work. Without it, you get "command not found"
			elseif has('unix')
				if filereadable('/usr/bin/mimeo')
					silent execute "!mimeo " . fnamemodify(mainfile,":p:r") . ".pdf &> /dev/null"
				else
					silent execute "!xdg-open " . fnamemodify(mainfile,":p:r") . ".pdf"
				endif
			endif
		endif
		" TODO: add support for other operating systems.
	endif
endfunction
"}}}
"}}}
" Experimental: remove tags like \emph{}. "{{{
function s:RemoveLaTeXTag()
	normal! diB
	" This should leave the cursor on the right brace
	" Just need to kill the empty tag now:
	if search("\\","sbW")
		normal! "_d``"_dl
	endif
	" now put the deleted inner block back:
	normal! P
endfunction
" TODO: this will fail when you delete a tag at the end of the line.
" The issue is that the "dl" command would normally leave the cursor to
" the right, but there is no space, so it moves adjacent to the prior text.
" TODO: you should probably make one that adds new tags as well.
"}}}
"}}}
" after functions have been loaded, default 
" the main file to the current file:
if !exists('g:latexMainFileName') || g:latexMainFileName == ""
	call s:setLatexMainFile()
endif
" Error message filtering "{{{

" Stuff stolen from Vim-Latex:
" This section contains the customization variables which the user can set.
" g:Tex_IgnoredWarnings: This variable contains a ¡ seperated list of
" patterns which will be ignored in the TeX compiler's output. Use this
" carefully, otherwise you might end up losing valuable information.

let g:Tex_IgnoreLevel = 4 "skip the first 4 warnings below
if !exists('g:Tex_IgnoredWarnings')
	let g:Tex_IgnoredWarnings =
		\'Underfull'."\n".
		\'Overfull'."\n".
		\'specifier changed to'."\n".
		\'You have requested'."\n".
		\'Missing number, treated as zero.'."\n".
		\'There were undefined references'."\n".
		\'Citation %.%# undefined'
endif
" This is the number of warnings in the g:Tex_IgnoredWarnings string which
" will be ignored.
if !exists('g:Tex_IgnoreLevel')
	let g:Tex_IgnoreLevel = 7
endif
" There will be lots of stuff in a typical compiler output which will
" completely fall through the 'efm' parsing. This options sets whether or not
" you will be shown those lines.
if !exists('g:Tex_IgnoreUnmatched')
	let g:Tex_IgnoreUnmatched = 1
endif
" With all this customization, there is a slight risk that you might be
" ignoring valid warnings or errors. Therefore before getting the final copy
" of your work, you might want to reset the 'efm' with this variable set to 1.
" With that value, all the lines from the compiler are shown irrespective of
" whether they match the error or warning patterns.
" NOTE: An easier way of resetting the 'efm' to show everything is to do
"       TCLevel strict
if !exists('g:Tex_ShowallLines')
	let g:Tex_ShowallLines = 0
endif

fun! <SID>Strntok(s, tok, n)
	return matchstr( a:s.a:tok[0], '\v(\zs([^'.a:tok.']*)\ze['.a:tok.']){'.a:n.'}')
endfun

function! <SID>IgnoreWarnings()
	let i = 1
	while s:Strntok(g:Tex_IgnoredWarnings, "\n", i) != '' &&
				\ i <= g:Tex_IgnoreLevel
		let warningPat = s:Strntok(g:Tex_IgnoredWarnings, "\n", i)
		let warningPat = escape(substitute(warningPat, '[\,]', '%\\\\&', 'g'), ' ')
		exe 'setlocal efm+=%-G%.%#'.warningPat.'%.%#'
		let i = i + 1
	endwhile
endfunction

function! <SID>SetLatexEfm()

	let pm = ( g:Tex_ShowallLines == 1 ? '+' : '-' )

	setlocal efm=

	if !g:Tex_ShowallLines
		call s:IgnoreWarnings()
	endif

	setlocal efm+=%E!\ LaTeX\ %trror:\ %m
	setlocal efm+=%E!\ %m
	setlocal efm+=%E%f:%l:\ %m

	setlocal efm+=%+WLaTeX\ %.%#Warning:\ %.%#line\ %l%.%#
	setlocal efm+=%+W%.%#\ at\ lines\ %l--%*\\d
	setlocal efm+=%+WLaTeX\ %.%#Warning:\ %m

	exec 'setlocal efm+=%'.pm.'Cl.%l\ %m'
	exec 'setlocal efm+=%'.pm.'Cl.%l\ '
	exec 'setlocal efm+=%'.pm.'C\ \ %m'
	exec 'setlocal efm+=%'.pm.'C%.%#-%.%#'
	exec 'setlocal efm+=%'.pm.'C%.%#[]%.%#'
	exec 'setlocal efm+=%'.pm.'C[]%.%#'
	exec 'setlocal efm+=%'.pm.'C%.%#%[{}\\]%.%#'
	exec 'setlocal efm+=%'.pm.'C<%.%#>%m'
	exec 'setlocal efm+=%'.pm.'C\ \ %m'
	exec 'setlocal efm+=%'.pm.'GSee\ the\ LaTeX%m'
	exec 'setlocal efm+=%'.pm.'GType\ \ H\ <return>%m'
	exec 'setlocal efm+=%'.pm.'G\ ...%.%#'
	exec 'setlocal efm+=%'.pm.'G%.%#\ (C)\ %.%#'
	exec 'setlocal efm+=%'.pm.'G(see\ the\ transcript%.%#)'
	exec 'setlocal efm+=%'.pm.'G\\s%#'
	exec 'setlocal efm+=%'.pm.'O(%*[^()])%r'
	exec 'setlocal efm+=%'.pm.'P(%f%r'
	exec 'setlocal efm+=%'.pm.'P\ %\\=(%f%r'
	exec 'setlocal efm+=%'.pm.'P%*[^()](%f%r'
	exec 'setlocal efm+=%'.pm.'P(%f%*[^()]'
	exec 'setlocal efm+=%'.pm.'P[%\\d%[^()]%#(%f%r'
	if g:Tex_IgnoreUnmatched && !g:Tex_ShowallLines
		setlocal efm+=%-P%*[^()]
	endif
	exec 'setlocal efm+=%'.pm.'Q)%r'
	exec 'setlocal efm+=%'.pm.'Q%*[^()])%r'
	exec 'setlocal efm+=%'.pm.'Q[%\\d%*[^()])%r'
	if g:Tex_IgnoreUnmatched && !g:Tex_ShowallLines
		setlocal efm+=%-Q%*[^()]
	endif
	if g:Tex_IgnoreUnmatched && !g:Tex_ShowallLines
		setlocal efm+=%-G%.%#
	endif

endfunction

"}}}
