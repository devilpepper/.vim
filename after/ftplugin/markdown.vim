" local settings"{{{
setlocal shiftwidth=4
setlocal foldmethod=marker
setlocal spell spelllang=en_us
setlocal textwidth=78
"}}}

" markdown-specific mappings:"{{{
" All mappings defined here are buffer-local, so the following
" statements need to be executed every time:
" surroundings"{{{
let b:surround_66 = "**\r**"
let b:surround_69 = "*\r*"
let b:surround_67 = "`\r`"
let b:surround_36 = "$\r$"

" visual mode:
xmap <buffer> <M-b> <Plug>VSurroundB
xmap <buffer> <M-c> <Plug>VSurroundC
xmap <buffer> <M-e> <Plug>VSurroundE
xmap <buffer> <M-m> <Plug>VSurround$

" normal mode:
nmap <buffer> <M-b> <Plug>YsurroundewB
nmap <buffer> <M-c> <Plug>YsurroundewC
nmap <buffer> <M-e> <Plug>YsurroundewE
nmap <buffer> <M-m> <Plug>Ysurroundew$

" insert mode:
imap <buffer> <M-b> <Plug>IsurroundB
imap <buffer> <M-c> <Plug>IsurroundC
imap <buffer> <M-e> <Plug>IsurroundE
imap <buffer> <M-m> <Plug>Isurround$
"}}}
" misc"{{{
" the <buffer> local maps seem to override what's in .vimrc.
inoremap <buffer> <F12> <C-O>:call <SID>toggleFormat()<CR>
let g:SuperTabContextDefaultCompletionType='<c-p>'
nnoremap yq yypVr-
nnoremap yQ yypVr=
"}}}
" compilation / makefiles"{{{
"nnoremap <buffer> <localleader>p :w<CR>:!make<CR>
nnoremap <buffer> <localleader>c :call <SID>runMarkdownMake(0,0)<CR>
nnoremap <buffer> <localleader>C :call <SID>runMarkdownMake(1,0)<CR><C-L>
nnoremap <buffer> <localleader><localleader>c :call <SID>runMarkdownMake(0,0,1)<CR>
nnoremap <buffer> <localleader>p :call <SID>runMarkdownMake(0,1)<CR>
nnoremap <buffer> <localleader>P :call <SID>runMarkdownMake(1,1)<CR><C-L>
nnoremap <buffer> <localleader><localleader>p :call <SID>runMarkdownMake(0,1,1)<CR>
"}}}
"}}}

if exists("g:loaded_after_markdown")
	finish
endif
let g:loaded_after_markdown = 1

" configuration variables"{{{
" default makefile:
" let s:defmakefile = expand('~/Documents/miscsvn/scripts/markdown/mkd_makefile')
if has("unix")
	let s:defmakefile = expand('~/repos/misc/makefiles/pandoc_makefile')
else
	let s:defmakefile = expand('~/Documents/miscsvn/scripts/markdown/pandoc_makefile')
endif
" TODO: might want to scan the current directory for a css file, and if found,
" try to use it as the css argument to the make file.
"}}}
" function definitions"{{{
function s:toggleFormat() "{{{
	let &textwidth = (&textwidth==78)?0:78
endfunction
"}}}
" Makefile stuff; setting the main file"{{{
" Main makefile execution / pdf display "{{{
function s:runMarkdownMake(showinbrowser,trylocal,...)
	" if startpdf is true, try to open the pdf after compiling.
	" if trylocal is true, attempt to use local makefile before the global
	" The third parameter, if present, asks to force compilation
	" by touching the main file before running make.
	silent update
	let mainfile = expand("%:t")
	if a:0 && a:1
		call system('touch ' . mainfile)
	endif
	let makestring = "make! -f " . s:defmakefile . " main=" . fnamemodify(mainfile,":r")
	if a:trylocal == 1 && filereadable("makefile")
		let makestring = "make!"
		echon "Compiling with local makefile"
	else
		echon "Compiling with " . s:defmakefile
	endif
	exe makestring
	if v:shell_error
		if len(getqflist()) > 0
			copen
			echohl WarningMsg
			echomsg "Build failed."
			echohl None
		endif
	else
		cclose
		redraw "get rid of annoying message about hitting return...
		echon "Build succeeded."
		if a:showinbrowser == 1
			if has('win32') || has('win64')
				silent execute "! start " . fnamemodify(mainfile,":p:r") . ".html"
			elseif has('unix')
				if filereadable('/usr/bin/mimeo')
					silent execute "!mimeo " . fnamemodify(mainfile,":p:r") . ".html &> /dev/null"
				else
					silent execute "!xdg-open " . fnamemodify(mainfile,":p:r") . ".html"
				endif
			endif
		endif
		" TODO: add support for other operating systems.
	endif
endfunction
"}}}
"}}}
"}}}
