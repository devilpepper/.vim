setlocal shiftwidth=4
setlocal number
" might want to try to read from some boilerplate file, but
" the following code does not work:
"source  c.vim
" Bram recently added ":" to the keyword chars.  I think this was a mistake.
setlocal iskeyword-=:
