" local settings:
setlocal sw=4

" inoremap <buffer> <F12> <C-O>:call <SID>cycleSupertabCompletionType()<CR>

" supertab configuration:
let g:SuperTabContextDefaultCompletionType='<c-x><c-o>'
" if you want to, you can try to write a context that figures
" out when a good time to attempt omni completion is, but I
" think it is usually the case in a css file.
" function s:cycleSupertabCompletionType()
" 	let stopts = {'<c-p>': '<c-x><c-o>', '<c-x><c-o>': '<c-p>'}
" 	let g:SuperTabContextDefaultCompletionType = 
" 				\ stopts[g:SuperTabContextDefaultCompletionType]
" endfunction
