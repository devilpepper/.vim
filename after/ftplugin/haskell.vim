setlocal softtabstop=4
setlocal expandtab

" keyboard mappings
inoremap <buffer> <M-t> <Space>-><Space>
inoremap <buffer> <S-M-t> <Space>=><Space>
inoremap <buffer> <M-e> <Space><-<Space>
nnoremap <buffer> <M-r> :GHCi<Space>

" Change supertab options?

" syntax
let hs_highlight_delimiters = 1
let hs_highlight_boolean = 1
let hs_highlight_types = 1
let hs_highlight_more_types = 1
let hs_highlight_debug = 1
