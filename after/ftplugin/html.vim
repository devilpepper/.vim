" local settings:
setlocal sw=4

" inoremap <buffer> <F12> <C-O>:call <SID>cycleSupertabCompletionType()<CR>

if exists("g:loaded_after_html")
	finish
endif
let g:loaded_after_html = 1

" FIXME: is there a way to avoid having this set for all buffers?
" Browsing source, it looks like no.  There is only a g: flavor
" of this variable.  See if new stuff is in git.
" supertab configuration:
let g:SuperTabContextDefaultCompletionType='<c-x><c-o>'
