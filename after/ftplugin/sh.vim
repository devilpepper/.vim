setlocal formatoptions+=r
setlocal textwidth=78

" some surroundings.
let b:surround_35 = "${\r}"  " variable expansion
" not sure why no one seems to use $[]...  it isn't even in the
" bash manual now.  For now we'll use $(()) as well.
let b:surround_36 = "$((\r))"  " for Value comparison expressions
let b:surround_37 = "$(\r)"  " for making variables out of commands
xmap <buffer> <M-m>   <Plug>VSurround#
xmap <buffer> <M-v>   <Plug>VSurround$
xmap <buffer> <S-M-m> <Plug>VSurround%
nmap <buffer> <M-m>   <Plug>Ysurroundew#
nmap <buffer> <M-v>   <Plug>Ysurroundew$
nmap <buffer> <S-M-m> <Plug>Ysurroundew%
imap <buffer> <M-m>   <Plug>Isurround#
imap <buffer> <M-v>   <Plug>Isurround$
imap <buffer> <S-M-m> <Plug>Isurround%

nnoremap <buffer> ,q =ap

" vim:sw=4:ff=unix:ft=vim

