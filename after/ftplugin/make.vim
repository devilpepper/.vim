" surroundings:
let b:surround_36 = "$(\r)"
xmap <buffer> <M-m> <Plug>VSurround$
nmap <buffer> <M-m>   <Plug>Ysurroundew$
imap <buffer> <M-m> <Plug>Isurround$

" vim:sw=4:ff=unix:ft=vim

