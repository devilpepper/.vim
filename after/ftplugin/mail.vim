" import some defaults from text files: "{{{
runtime! ftplugin/text.vim ftplugin/text_*.vim ftplugin/text/*.vim
set commentstring=>\ %s
setlocal tw=72
setlocal foldmethod=syntax
setlocal foldlevel=1
nnoremap <buffer> <silent> <localleader>w :read ~/.vim/resources/sig-short.mail<CR>
nnoremap <buffer> <silent> <localleader>W :read ~/.vim/resources/sig.mail<CR>
"}}}
