" keyboard mappings
inoremap <buffer> <M-t> <Space>-><Space>
nnoremap <buffer> <M-F5> :silent execute "!dot % -Tsvg -o " . expand("%:r") . ".svg"<CR><CR>
command! -buffer -nargs=0 RunDot execute "!dot % -Tsvg -o " . expand("%:r") . ".svg"
setlocal formatoptions=crql

" comments via surround.vim {{{
let b:surround_67 = "/* \r */"

" visual mode:
xmap <buffer> <M-c> <Plug>VSurroundC

" normal mode:
nmap <buffer> <M-c> <Plug>YsurroundewC

" insert mode:
imap <buffer> <M-c> <Plug>IsurroundC
"}}}
