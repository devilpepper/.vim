setlocal foldmethod=indent
setlocal foldnestmax=2
setlocal softtabstop=4
setlocal expandtab
setlocal formatoptions+=r
setlocal foldignore=''

" keyboard mappings
nnoremap <buffer> <F5> :silent execute "!python %"<CR><CR>
if has('win32')
	nnoremap <buffer> <M-F5> :!start cmd /c "python -i %"<CR>
else
	nnoremap <buffer> <M-F5> :silent execute "!python -i %"<CR><CR>
endif
