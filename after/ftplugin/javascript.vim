setlocal formatoptions-=o

" comments via surround.vim {{{
let b:surround_67 = "/* \r */"

" visual mode:
xmap <buffer> <M-c> <Plug>VSurroundC

" normal mode:
nmap <buffer> <M-c> <Plug>YsurroundewC

" insert mode:
imap <buffer> <M-c> <Plug>IsurroundC
"}}}

" vim:sw=4:ff=unix:ft=vim

