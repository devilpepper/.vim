setlocal shiftwidth=4
setlocal foldmethod=syntax
setlocal formatoptions-=o
" to exclude documentation directories:
setlocal grepprg=ack\ --ignore-dir=html

nnoremap <buffer> ,q =ap

" surroundings {{{
let b:surround_67 = "/* \r */"

" visual mode:
xmap <buffer> <M-c> <Plug>VSurroundC

" normal mode:
nmap <buffer> <M-c> <Plug>YsurroundewC

" insert mode:
imap <buffer> <M-c> <Plug>IsurroundC
"}}}

" prevent duplicate function definitions:
if exists("g:loaded_after_php")
	finish
endif
let g:loaded_after_php = 1
