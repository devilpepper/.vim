" Filetype detection
if exists("did_load_filetypes")
	finish
endif
augroup filetypedetect
	au! BufRead,BufNewFile *.txt		setfiletype text
	au! BufRead,BufNewFile *.text		setfiletype text

	au! BufRead,BufNewFile *.tex		setfiletype tex
	au! BufRead,BufNewFile *.ltx		setfiletype tex

	au! BufRead,BufNewFile *.pde		setfiletype arduino
	
	au! BufRead,BufNewFile *.mail		setfiletype mail

	au! BufRead,BufNewFile *.gap		setfiletype gap
	au! BufRead,BufNewFile *.g			setfiletype gap
	au! BufRead,BufNewFile *.gi 		setfiletype gap
	au! BufRead,BufNewFile *.gd 		setfiletype gap
	au! BufRead,BufNewFile .gaprc 		setfiletype gap

	au! BufRead,BufNewFile *.maple 		setfiletype maple

	au! BufRead,BufNewFile /tmp/bash-fc* setfiletype sh
augroup END
