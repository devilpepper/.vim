" LaTeX Box mappings

if exists("g:LatexBox_no_mappings")
	finish
endif

" latexmk {{{
if 0
map <buffer> <LocalLeader>ll :Latexmk<CR>
map <buffer> <LocalLeader>lL :LatexmkForce<CR>
map <buffer> <LocalLeader>lc :LatexmkClean<CR>
map <buffer> <LocalLeader>lC :LatexmkCleanAll<CR>
map <buffer> <LocalLeader>lg :LatexmkStatus<CR>
map <buffer> <LocalLeader>lG :LatexmkStatusDetailed<CR>
map <buffer> <LocalLeader>lk :LatexmkStop<CR>
map <buffer> <LocalLeader>le :LatexErrors<CR>
endif
" }}}

" View {{{
" map <buffer> <LocalLeader>lv :LatexView<CR>
" }}}

" Error Format {{{
" This assumes we're using the -file-line-error with [pdf]latex.
setlocal efm=%E%f:%l:%m,%-Cl.%l\ %m,%-G
" }}}

" TOC {{{
command! LatexTOC call LatexBox_TOC()
map <silent> <buffer> <LocalLeader>lt :LatexTOC<CR>
" }}}

" begin/end pairs {{{
" nmap <buffer> % <Plug>LatexBox_JumpToMatch
" xmap <buffer> % <Plug>LatexBox_JumpToMatch
" vmap <buffer> ee <Plug>LatexBox_SelectCurrentEnvInner
" vmap <buffer> ae <Plug>LatexBox_SelectCurrentEnvOuter
" omap <buffer> ee :normal vee<CR>
" omap <buffer> ae :normal vae<CR>
" vmap <buffer> em <Plug>LatexBox_SelectInlineMathInner
" vmap <buffer> am <Plug>LatexBox_SelectInlineMathOuter
" omap <buffer> em :normal vem<CR>
" omap <buffer> am :normal vam<CR>
" }}}

setlocal omnifunc=LatexBox_Complete

finish

" Suggested mappings:

" Motion {{{
noremap <silent> <buffer> <M-9> :call LatexBox_JumpToNextBraces(0)<CR>
noremap <silent> <buffer> <M-0> :call LatexBox_JumpToNextBraces(1)<CR>
imap <silent> <buffer> <M-9> <C-R>=LatexBox_JumpToNextBraces(0)<CR>
imap <silent> <buffer> <M-0> <C-R>=LatexBox_JumpToNextBraces(1)<CR>
" }}}

" vim:fdm=marker:ff=unix:noet:ts=4:sw=4
